export function getFillings(sandwich) {
  if (sandwich === undefined) throw new Error("ingredients is required");
  return sandwich.fillings;
}

export function isFromManchester(person) {
  if (person === undefined) throw new Error("person is required");
  //Returns true if person.city is Manchester
  //otherwise false
  return person.city === "Manchester";
}

export function getBusNumbers(people) {
  if (people === undefined) throw new Error("people is required");
  // returns 1 if the people <= 40
  // returns people/40 
  return people <= 40 ? 1 : Math.ceil(people/40);
}

export function countSheep(arr) {
  if (arr === undefined) throw new Error("arr is required");
  const isSheepFilter = arr.filter(item => item === "sheep");
  return isSheepFilter === undefined ? 0: isSheepFilter.length;
}

export function hasMPostCode(person) {
  if (person === undefined) throw new Error("person is required");
  //filters based on PostCode starts with M
  const postCodeFilter = person.address.postCode.startsWith('M');
  //filters based on Manchester or not
  const cityFilter =  person.address.city ==='Manchester';
  return postCodeFilter && cityFilter;
}
