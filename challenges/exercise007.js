/**
 * This function takes a number, e.g. 123 and returns the sum of all its digits, e.g 6 in this example.
 * @param {Number} n
 */
export const sumDigits = (n) => {
  if (n === undefined) throw new Error("n is required");
  let sumOfDigits = 0;
  let input = n;
  while (input > 0)
  {
    const remainder = input%10;
    sumOfDigits += remainder;
    input = Math.floor(input/10);
  }
  return sumOfDigits;
};


/**
 * This function creates a range of numbers as an array. It received a start, an end and a step. Step is the gap between numbers in the range. For example, if start = 3, end = 11 and step = 2 the resulting range would be: [3, 5, 7, 9, 11]
 * Both the start and the end numbers are inclusive.
 * Step is an optional parameter. If it is not provided, assume the step is 1.
 * @param {Number} start
 * @param {Number} end
 * @param {Number} step
 */
export const createRange = (start, end, step) => {
  if (start === undefined) throw new Error("start is required");
  if (end === undefined) throw new Error("end is required");
  let inputStep = step || 1;
  let retArray = [];
  let loopVariable = start;
  while (loopVariable <= end)
  {
    retArray.push(loopVariable);
    loopVariable = loopVariable + inputStep;
  }   
  return retArray;
};

/**
 * This function takes an array of user objects and their usage in minutes of various applications. The format of the data should be as follows:
 * [
 *  {
 *    username: "beth_1234",
 *    name: "Beth Smith",
 *    screenTime: [
 *                 { date: "2019-05-01", usage: { twitter: 34, instagram: 22, facebook: 40} },
 *                 { date: "2019-05-02", usage: { twitter: 56, instagram: 40, facebook: 31} },
 *                 { date: "2019-05-03", usage: { twitter: 12, instagram: 15, facebook: 19} },
 *                 { date: "2019-05-04", usage: { twitter: 10, instagram: 56, facebook: 61} },
 *                ]
 *   },
 *   {
 *    username: "sam_j_1989",
 *    name: "Sam Jones",
 *    screenTime: [
 *                 { date: "2019-06-11", usage: { mapMyRun: 0, whatsApp: 0, facebook: 0, safari: 10} },
 *                 { date: "2019-06-13", usage: { mapMyRun: 0, whatsApp: 0, facebook: 0, safari: 16} },
 *                 { date: "2019-06-14", usage: { mapMyRun: 0, whatsApp: 0, facebook: 0, safari: 31} },
 *                ]
 *   },
 * ]
 *
 * The function should return an array of usernames of users who have used more than 100 minutes of screentime for a given date.
 * The date will be provided in the format "2019-05-04" (YYYY-MM-DD)
 * For example, if passed the above users and the date "2019-05-04" the function should return ["beth_1234"] as she used over 100 minutes of screentime on that date.
 * @param {Array} users
 */
export const getScreentimeAlertList = (users, date) => {
  if (users === undefined) throw new Error("users is required");
  if (date === undefined) throw new Error("date is required");
  let userAlertList = [];
    const userScreenTimeout = users.filter((element) => {
    const scre = element.screenTime.some((ele) => {
      
    const usageTotal = Object.values(ele.usage).reduce((totalValue,item) => {
            totalValue += item;
            return totalValue;
        },0);
    return ele.date === date && usageTotal > 100;


    });
    return scre;
    });
    userScreenTimeout.forEach(element => {
    userAlertList.push(element.name);
    });
  return userAlertList;
};

/**
 * This function will receive a hexadecimal color code in the format #FF1133. A hexadecimal code is a number written in hexadecimal notation, i.e. base 16. If you want to know more about hexadecimal notation:
 * https://www.youtube.com/watch?v=u_atXp-NF6w
 * For colour codes, the first 2 chars (FF in this case) represent the amount of red, the next 2 chars (11) represent the amound of green, and the last 2 chars (33) represent the amount of blue.
 * Colours can also be represented in RGB format, using decimal notation.
 * This function should transform the hex code into an RGB code in the format:
 * "rgb(255,17,51)"
 * Hint: You will need to convert each hexadecimal value for R, G and B into its decimal equivalent!
 * @param {String} str
 */
export const hexToRGB = (hexStr) => {
  if (hexStr === undefined) throw new Error("hexStr is required");

  const inputRedStr = hexStr.substring(1,3);
  const inputGreenStr = hexStr.substring(3,5);
  const inputBlueStr = hexStr.substring(5);

  //hex to decimal
  const hexaToDecValue = function hexaToDecimal(inp, pos) {
  
  function computeValue(decimalValue)
  {
    return charPosition * decimalValue;
  }
  let charPosition;
  //used in computeValue function
  pos === 1 ? charPosition = 1 : charPosition = 16;  
  const hexatoDecimalObject = {0:0,1:1,2:2,3:3,4:4,5:5,6:6,7:7,
                               8:8,9:9,'A':10, 'B':11, 'C':12,
                               'D':13, 'E':14, 'F':15};
  return computeValue(hexatoDecimalObject[inp]);
  
}
const redRGBValue = hexaToDecValue(inputRedStr[0],2) + hexaToDecValue(inputRedStr[1],1);
const greenRGBValue = hexaToDecValue(inputGreenStr[0],2) + hexaToDecValue(inputGreenStr[1],1);
const blueRGBValue = hexaToDecValue(inputBlueStr[0],2) + hexaToDecValue(inputBlueStr[1],1);
return "rgb("+redRGBValue+","+greenRGBValue+","+blueRGBValue+")";

};

/**
 * This function takes a noughts and crosses board represented as an array, where an empty space is represented with null.
 * [
 *  ["X", "0", null],
 *  ["X", null, "0"],
 *  ["X", null, "0"]
 * ]
 * The function should return "X" if player X has won, "0" if the player 0 has won, and null if there is currently no winner.
 * @param {Array} board
 */
export const findWinner = (board) => {
  if (board === undefined) throw new Error("board is required");

  let player;
  let resXOutput;
  let res0Output;
  let firstMatch;
  let secondMatch;
  let thirdMatch;

  function trysolution(rowinput, colinput) 
  {
    if ((rowinput >= board.length)  || (colinput >= board.length) || (rowinput < 0) || (colinput < 0))
      return false;
    else if (board[rowinput][colinput] === player) //&& ((rowinput === board.length-1)) || (colinput === board.length-1))
    {
      if (rowinput === 0  || colinput === 0)
        firstMatch = true;
      if (rowinput === 1 || colinput === 1)
        secondMatch = true;
      if (rowinput === 2 || colinput === 2)
        thirdMatch = true;
      
      if (firstMatch && secondMatch  && thirdMatch)   
      {  
        return true;  
      } 
      return(trysolution(rowinput+1,colinput) || trysolution(rowinput,colinput+1) || trysolution(rowinput+1,colinput+1) || trysolution(rowinput-1,colinput-1));
   }
}
  
  if (board === undefined) throw new Error("board is required");
    if (board.length === 3)
    {
        player = "X";
        resXOutput = trysolution(0,0);
        player = "0"
        res0Output = trysolution(0,0);
        if (resXOutput)
        {
          return "X";
        }
        if (res0Output)
        {
          return "0";
        }
        if (!res0Output && !resXOutput)
        {
          return null;
        }
    }
};
