/**
 * This function will receive an array of numbers and should return the sum
 * of any numbers which are a multiple of 3 or 5
 * @param {Array} arr
 * @returns {Number}
 */
export const sumMultiples = (arr) => {
  if (arr === undefined) throw new Error("arr is required");
  const sumVal = arr.reduce(
    (sumValue, element) => {
    if ((element % 3 === 0) || (element % 5 === 0))
          sumValue += element;
  return sumValue;
  },0);
  return sumVal;
};

/**
 * This function will receive a string of characters and should return true/false depending on whether it is a valid DNA string. A valid DNA string may contain characters C, G, T or A only.
 * @param {String} str
 * @returns {Boolean}
 */
export const isValidDNA = (str) => {
  if (str === undefined) throw new Error("str is required");
  const strPattern = new RegExp(/^[cgta]+$/,'gi');
  const matchPattern = strPattern.test(str);
  return matchPattern;
};

/**
 * This function will receive a valid DNA string (see above) 
 * and should return a string of the complementary base pairs. 
 * In DNA, T always pairs with A, and C always pairs with G. So a string of "ACTG" would have a complementary DNA string of "TGAC".
 * @param {String} str
 * @returns {String}
 */
export const getComplementaryDNA = (str) => {
  if (str === undefined) throw new Error("str is required");
  const strSplit = str.split('');
  const complementaryDNA = { T: 'A', A: 'T', G: 'C', C: 'G', t: 'a', a: 't', g: 'c', c: 'g' };
  const complementedDNA = strSplit.map(ele => {
  return complementaryDNA[ele]
});
const reduceString = complementedDNA.reduce((res,Element) => res + Element  ,"");
return reduceString;
};

/**
 * This function should receive a number and return true/false depending on whether it is a prime number or not. A prime number is a number that can only be divided evenly by 1 and itself (for example, 7)
 * @param {Number} n
 * @returns {Boolean}
 */
export const isItPrime = (n) => {
  if (n === undefined) throw new Error("n is required");
  if (n < 2) return false;
  if ((n == 2) || (n == 3)) return true;
  if ((n % 2 == 0) || (n % 3 ==0)) return false;
  const sqrtn = Math.floor(Math.pow(n,0.5));
  for ( let i = 5; i * i <= n; i+=6)
  {
    if (n % i == 0 || n % (i + 2) == 0)
      return false;
  }
  return true;
};

/**
 * This function should receive a number and return an array of n arrays, each filled with n items. The parameter "fill" should be used as the filler of the arrays. For example, given parameters 3 and "foo" the resulting matrix should be:
 * [
 *   ["foo", "foo", "foo"],
 *   ["foo", "foo", "foo"],
 *   ["foo", "foo", "foo"]
 * ]
 * @param {Number} n
 * @param {Any} fill
 * @returns {Array}
 */
export const createMatrix = (n, fill) => {
  if (n === undefined) throw new Error("n is required");
  if (fill === undefined) throw new Error("fill is required");
  let arrayN ; 
  if (n == 1 || n == 2)
    arrayN = Array(n).fill(fill);
  else if (n ==3 )
    arrayN = Array(n).fill( Array(n).fill(fill));
  return arrayN;
};

/**
 * This function takes an array of staff objects in the format:
 * [
 *  { name: "Sally", rota: ["Monday", "Tuesday", "Friday"] },
 *  { name: "Pedro", rota: ["Saturday", "Sunday", "Tuesday", "Wednesday"] },
 *  ...etc
 * ]
 * and a day of the week. For the café to run successfully, at least 3 staff members are required per day. The function should return true/false depending on whether there are enough staff scheduled for the given day.
 * @param {Array} staff
 * @param {String} day
 * @returns {Boolean}
 */
export const areWeCovered = (staff, day) => {
  if (staff === undefined) throw new Error("staff is required");
  if (day === undefined) throw new Error("day is required");
  const staffDayFilter = staff.filter(element => {
  return element.rota.includes(day)});
  return staffDayFilter.length >= 3 ? true:false;
};
