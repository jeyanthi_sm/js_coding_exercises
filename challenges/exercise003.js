export function getSquares(nums) {
  if (nums === undefined) throw new Error("nums is required");
  //code accepts an array of numbers
  //returns its squares
  let squaresArray = nums.map((item) => item * item);
  return squaresArray;
}

export function camelCaseWords(words) {
  if (words === undefined) throw new Error("words is required");
  //accepts sets of words
  //returns joined words with the first letter as capital
  
  let returnWords = "";
  /*returnWords = words.reduce(function returnWordcbfn(joinedWords,currentVal) {
    return returnWords === "" ? returnWords = currentVal[0].toLowerCase() + currentVal.substr(1):
       returnWords += currentVal[0].toUpperCase()+ currentVal.substr(1);
     },returnWords);
   return returnWords;
   */
  return words.reduce(function returnWordcbfn(joinedWords,currentVal) {
    return returnWords === "" ? returnWords = currentVal[0].toLowerCase() + currentVal.substr(1):
      returnWords += currentVal[0].toUpperCase()+ currentVal.substr(1);
    },returnWords);
  

}

export function getTotalSubjects(people) {
  if (people === undefined) throw new Error("people is required");
  let totalSubjects = 0;
  //looping thorugh people array
  people.forEach(item => {
    //checking subjects has any element
  if (item.subjects.length > 0)
    //counting the subjects
    item.subjects.forEach(sub => totalSubjects += 1);
  });
  return totalSubjects;
}

export function checkIngredients(menu, ingredient) {
  if (menu === undefined) throw new Error("menu is required");
  if (!ingredient) throw new Error("ingredient is required");
    //check for presence of ingredients
    return menu.find(recipe => recipe.ingredients.includes(ingredient)) !== undefined;
}

export function duplicateNumbers(arr1, arr2) {
  if (arr1 === undefined) throw new Error("arr1 is required");
  if (arr2 === undefined) throw new Error("arr2 is required");
  //compare numbers returns -1 if a < b
  //returns 0 if they are equal
  //returns 1 if a > b
  function compareNumbers(a,b){
    return a-b;
  }
  let dupArray = [];
  //looping through each element of Array 1
  arr1.forEach(element => {
    //findining the element in Array2
  if (arr2.find(elem => elem === element) != undefined)
    //checking for duplicates before inserting the dupArray
    dupArray.find(ele => ele === element) ? undefined:dupArray.push(element);
  });
  dupArray.sort(compareNumbers);
  return dupArray;
}
