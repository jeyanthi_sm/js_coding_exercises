export const findNextNumber = (nums, n) => {
  if (nums === undefined) throw new Error("nums is required");
  if (n === undefined) throw new Error("n is required");
  const findMatchingIndex = nums.findIndex(elem => elem === n);
  if ((findMatchingIndex === -1) || (findMatchingIndex === nums.length-1))
    return null; 
  else
    return nums[findMatchingIndex+1];  
};

export const count1sand0s = (str) => {
  if (str === undefined) throw new Error("str is required");
  //spliting the str by individual characters
  //spliting the str by individual characters
  const strSplit = str.split('');
  const filter1s = strSplit.filter(x => x==="1");
  const filter0s = strSplit.filter(x => x==="0");
  return ({1:filter1s.length,0:filter0s.length});
};

export const reverseNumber = (n) => {
  if (n === undefined) throw new Error("n is required");
  let inpVal=n;
  let revNumber = 0;
  while (inpVal>0)
  {
    let remainder = inpVal%10;
    revNumber = revNumber*10+remainder;
    inpVal = Math.floor(inpVal/10);
  }
  return revNumber;
};

export const sumArrays = (arrs) => {
  if (arrs === undefined) throw new Error("arrs is required");
    const totalSumOfArrays = arrs.reduce((allTotal, individualArray) => {
    const individualSum = individualArray.reduce((individulTotal, ele) => {
    individulTotal += ele;
    return individulTotal;
    }, 0);
    allTotal += individualSum;
    return allTotal;
  }, 0);
return totalSumOfArrays;

};

export const arrShift = (arr) => {
  if (arr === undefined) throw new Error("arr is required");
  //shift the array element 0 to 1 and n to 0 and so on
  const retArray = arr.map((x,index,arr) => {
    if (index === 0)  {x = arr[arr.length-1]; return x;}
    else if (index === arr.length-1)  {x = arr[0]; return x;}    
    else {x = x; return x;}
});
return retArray;
};

export const findNeedle = (haystack, searchTerm) => {
  if (haystack === undefined) throw new Error("haystack is required");
  if (searchTerm === undefined) throw new Error("searchTerm is required");
  //to check for case insensitve searchterm 
  //so modifiying to Regular expression pattern
  const searchPattern = new RegExp(searchTerm, "i");
  let matchedValues=false;
  //looping through each item in objects to find the pattern
  //if matched then the loop is break and retured true
  //after finishing if there is no match return false
  for (const obj in haystack) 
  {
    let objValue = haystack[obj];
    if ((typeof objValue)  === "string")
    {
      matchedValues = searchPattern.test(objValue);
      if (matchedValues) break; 
    }
  }
  return matchedValues? matchedValues:false;
  };

export const getWordFrequencies = (str) => {
  if (str === undefined) throw new Error("str is required");
//splitting the word by space
//let strSplitArray = str.split(' ');
let strSplitArray = str.split(/\.|\s|,|!|\?/); 
//Map to store the words with frequency count
let strSplitArrayMap = new Map();
strSplitArray.forEach(element => {
  if ((element.length) >  0) 
  {
    const elementLowercase = element.toLowerCase();
    const matchedkey = strSplitArrayMap.get(elementLowercase);
    strSplitArrayMap.get(elementLowercase) === undefined ? strSplitArrayMap.set(elementLowercase,1):strSplitArrayMap.set(elementLowercase,matchedkey+1);
  };
});

let resObject =  Object.fromEntries(strSplitArrayMap);
return resObject;


};
