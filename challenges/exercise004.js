export function findSmallNums(nums) {
  if (!nums) throw new Error("nums is required");
  //returning smallNums from the list of numbers
  return nums.filter(element => element < 1.0);
}

export function findNamesBeginningWith(names, char) {
  if (!names) throw new Error("names is required");
  if (!char) throw new Error("char is required");
  //finding names Beginning with the passed character
  return names.filter(elem => elem.startsWith(char));
}

export function findVerbs(words) {
  if (!words) throw new Error("words is required");
  //filtering only the verbs in the set of words
  return words.filter(elem => elem.startsWith('to '));
  
}

export function getIntegers(nums) {
  if (!nums) throw new Error("nums is required");
  //retuning the integer values of the array
  return nums.filter(elem => Number.isInteger(elem));
}

export function getCities(users) {
  if (!users) throw new Error("users is required");
 //to get the displayName for the CitiesArray
  let getCitiesArray = [];
  users.forEach(el => { getCitiesArray.push(el.data.city.displayName); });
  return getCitiesArray;
}

export function getSquareRoots(nums) {
  if (!nums) throw new Error("nums is required");
  //returns the squareroot of each array element
  return nums.map(element => Number(Math.pow(element,0.5).toFixed(2)));
}

export function findSentencesContaining(sentences, str) {
  if (!sentences) throw new Error("sentences is required");
  if (!str) throw new Error("str is required");
  //finding sentences that has the input str
  return sentences.filter(ele => ele.toLowerCase().includes(str));
}

export function getLongestSides(triangles) {
  if (!triangles) throw new Error("triangles is required");
  //returning the longest sides in the set of triagles
  return triangles.map(elem => elem.reduce(
    (maxValue, elem) => ((elem > maxValue)? elem: maxValue),0)
    ); 
}
