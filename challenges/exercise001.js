// Note: Be sure to check out all the exercises corresponding .md files (in docs)! 📘 👍

export function capitalize(word) {
  if (word === undefined) throw new Error("word is required");
    return word[0].toUpperCase() + word.substr(1,word.length);
}

export function generateInitials(firstName, lastName) {
  if (firstName === undefined) throw new Error("firstName is required");
  if (lastName === undefined) throw new Error("lastName is required");
  return firstName[0]+ '.'+lastName[0];
}

export function addVAT(originalPrice, vatRate) {
  if (originalPrice === undefined) throw new Error("originalPrice is requied");
  if (vatRate === undefined) throw new Error("vatRate is required");
    const VAT = (originalPrice * vatRate) /100;
    const PriceAfterVAT = originalPrice + VAT;
    return Number.isInteger(PriceAfterVAT) ? PriceAfterVAT : Number(PriceAfterVAT.toFixed(2)); 
}

export function getSalePrice(originalPrice, reduction) {
  if (originalPrice === undefined) throw new Error("originalPrice is required");
  if (reduction === undefined) throw new Error("reduction is required");
   return originalPrice - ((originalPrice * reduction).toFixed()/100);
}

export function getMiddleCharacter(str) {
  if (str === undefined) throw new Error("str is required");
  return str.length % 2 === 0 ? (str.substr((str.length/2)-1,2)) :
                                (str.substr(Math.floor(str.length/2),1));
}

export function reverseWord(word) {
  if (word === undefined) throw new Error("word is required");
  const wordReversedArray = word.split("").map((element,index,word) => word[word.length-index-1]);
  const wordReversed = wordReversedArray.reduce((retreversedword, element) => retreversedword + element, "");
  return wordReversed; 
}

export function reverseAllWords(words) {
  if (words === undefined) throw new Error("words is required");
  let retArr = words.map(x => reverseWord(x));
  return retArr;
}

export function countLinuxUsers(users) {
  if (users === undefined) throw new Error("users is required");
   //check for type = Linux
  return users.filter (user => user["type"] === "Linux").length;  
}

export function getMeanScore(scores) {
  if (scores === undefined) throw new Error("scores is required");
  const meanScore = (scores.reduce((scoreSum, element) => scoreSum += element)/scores.length);
  return Number.isInteger(meanScore) ? meanScore : Number(meanScore.toFixed(2));
  
}

export function simpleFizzBuzz(n) {
  if (n === undefined) throw new Error("n is required");
  // returns 'fizz' if the number is divisible by 3
  let retValue = "";
  if (n%3 === 0)
    retValue = "fizz";
  //returns buzz if it divisible by 5
  if (n%5 === 0) 
    retValue += "buzz";
  if (retValue === "")
    return n;

return retValue;
}
