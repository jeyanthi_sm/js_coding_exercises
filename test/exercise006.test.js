import {
    sumMultiples,
    isValidDNA,
    getComplementaryDNA,
    isItPrime,
    createMatrix,
    areWeCovered,
} from "../challenges/exercise006";

describe("sumMultiples", () => {
    test("sum of Multiples of 3 and 5", () => {
    expect(sumMultiples([5])).toBe(5);
    expect(sumMultiples([3,4,5])).toBe(8);
    expect(sumMultiples([3,5,6,10,15,12])).toBe(51);
    expect(sumMultiples([1,2,4,7])).toBe(0);
    });
});

describe("isValidDNA", () => {
    test("Test for Valid DNA", () => {
        expect(isValidDNA("")).toBe(false);
        expect(isValidDNA("CGTA")).toBe(true);
        expect(isValidDNA("gtaccccccccc")).toBe(true);
        expect(isValidDNA("aaaaaaagtaccccccccc")).toBe(true);
        expect(isValidDNA("bztxgtaccccccccc")).toBe(false);
        expect(isValidDNA("bbbbbbbbbbbztxgtaccccccccc")).toBe(false);
    });
});


describe("getComplementaryDNA", () => {
    test("Test for Coplementary DNA", () => {
        expect(getComplementaryDNA("")).toBe("");
        expect(getComplementaryDNA("CGTA")).toBe("GCAT");
        expect(getComplementaryDNA("gtaccccccccc")).toBe("catggggggggg");
        expect(getComplementaryDNA("aaaaaaagtaccccccccc")).toBe("tttttttcatggggggggg");
    });
});

describe("isItPrime", () => {
    test("Test for Number is prime or not", () => {
        expect(isItPrime(5)).toBe(true);
        expect(isItPrime(1297)).toBe(true);
        expect(isItPrime(11087)).toBe(true);
        expect(isItPrime(10087)).toBe(false);
        expect(isItPrime(4)).toBe(false);
        expect(isItPrime(997)).toBe(true);
        expect(isItPrime(929)).toBe(true);
        expect(isItPrime(931)).toBe(false);
    });
});

describe("createMatrix", () => {
    test("Test for createMatrix 1 * 1", () => {
        const outputArray = ["foo"];
        expect(createMatrix(1,"foo")).toEqual(outputArray)
        expect(createMatrix(1,"foo")).not.toBe(outputArray);

    });
   /* test("Test for createMatrix 2 * 2", () => {
        const outputArray = [["foo","foo"],["foo","foo"]];
        expect(createMatrix(2,"foo")).toEqual(outputArray)
        expect(createMatrix(2,"foo")).not.toBe(outputArray);
    });
*/
    test("Test for createMatrix 3 * 3", () => {
        const outputArray = [["foo","foo","foo"],["foo","foo","foo"],["foo","foo","foo"]];
        expect(createMatrix(3,"foo")).toEqual(outputArray)
        expect(createMatrix(3,"foo")).not.toBe(outputArray);
    });
});

describe("areWeCovered", () => {
    test("Test for areWeCovered", () => {
        expect(areWeCovered([
        { name: "Sally", rota: ["Monday", "Tuesday", "Friday"] },
        { name: "Pedro", rota: ["Saturday", "Sunday", "Tuesday", "Wednesday"] },
        { name: "Boss",  rota: ["Tuesday", "Thursday", "Friday"] },
        { name: "Tom",   rota: ["Wednesday", "Saturday", "Sunday"] },
        { name: "Dick",  rota: ["Saturday", "Sunday"] },
    ],"Tuesday")).toBe(true);

    expect(areWeCovered([
        { name: "Sally", rota: ["Monday", "Tuesday", "Friday"] },
        { name: "Pedro", rota: ["Saturday", "Sunday", "Tuesday", "Wednesday"] },
        { name: "Boss",  rota: ["Tuesday", "Thursday", "Friday"] },
        { name: "Tom",   rota: ["Wednesday", "Saturday", "Sunday"] },
    ],"Monday")).toBe(false);

    
});
});

/*


 aa = areWeCovered([
  { name: "Sally", rota: ["Monday", "Tuesday", "Friday"] },
  { name: "Pedro", rota: ["Saturday", "Sunday", "Tuesday", "Wednesday"] },
  { name: "Boss",  rota: ["Tuesday", "Thursday", "Friday"] },
  { name: "Tom",   rota: ["Wednesday", "Saturday", "Sunday"] },
],"Monday");



checkPrime = isItPrime(5);
checkPrime = isItPrime(1297);
checkPrime = isItPrime(10087);
checkPrime = isItPrime(4);

  test("returns the duplicate numbers in ascending order", () => {
    let arr1 = [1, 55, 4, 3, 7, 8];
    let arr2 = [55, 23, 65, 0, 1];
    expect(duplicateNumbers(arr1, arr2)).toEqual([1, 55]);*/

  