import {
    sumDigits,
    createRange,
    getScreentimeAlertList,
    hexToRGB,
    findWinner,
} from "../challenges/exercise007";

describe("sumDigits", () => {
    test("sum of Digits of n", () => {
    expect(sumDigits(5)).toBe(5);
    expect(sumDigits(123)).toBe(6);
    expect(sumDigits(356)).toBe(14);
    expect(sumDigits(0)).toBe(0);
    });
});
describe("createRange", () => {
    test("create Range of numbers step 2", () => {
        const outputArray = [3,5,7];
    expect(createRange(3,7,2)).toEqual(outputArray)
    expect(createRange(3,7,2)).not.toBe(outputArray);
    });
    test("create Range of numbers step 1", () => {
    const outputArray = [3,4,5,6,7];
    expect(createRange(3,7)).toEqual(outputArray)
    expect(createRange(3,7)).not.toBe(outputArray);
    });

    });
describe("getScreentimeAlertList", () => {
    test("getScreentimeAlertList matching records", () => {
            const getScreentimeOutputArray = [ 'Beth Smith' ]; 
            const inputArray = 
[
{
username: "beth_1234",
name: "Beth Smith",
screenTime: [
{ date: "2019-05-01", usage: { twitter: 34, instagram: 22, facebook: 40} },
{ date: "2019-06-11", usage: { twitter: 56, instagram: 40, facebook: 31} },
{ date: "2019-05-03", usage: { twitter: 12, instagram: 15, facebook: 19} },
{ date: "2019-05-04", usage: { twitter: 10, instagram: 56, facebook: 61} },
]
},
{
username: "sam_j_1989",
name: "Sam Jones",
screenTime: [
{ date: "2019-06-11", usage: { mapMyRun: 100, whatsApp: 0, facebook: 0, safari: 10} },
{ date: "2019-06-13", usage: { mapMyRun: 30, whatsApp: 0, facebook: 0, safari: 16} },
{ date: "2019-06-14", usage: { mapMyRun: 20, whatsApp: 0, facebook: 0, safari: 31} },
]
},
];
    expect(getScreentimeAlertList(inputArray,"2019-05-04")).toEqual(getScreentimeOutputArray)
    expect(getScreentimeAlertList(inputArray,"2019-05-04")).not.toBe(getScreentimeOutputArray);
    });

test("getScreentimeAlertList no matching records", () => {
    const inputArray = 
[
{
username: "beth_1234",
name: "Beth Smith",
screenTime: [
{ date: "2019-05-01", usage: { twitter: 34, instagram: 22, facebook: 40} },
{ date: "2019-06-11", usage: { twitter: 56, instagram: 40, facebook: 31} },
{ date: "2019-05-03", usage: { twitter: 12, instagram: 15, facebook: 19} },
{ date: "2019-05-04", usage: { twitter: 10, instagram: 56, facebook: 61} },
]
},
{
username: "sam_j_1989",
name: "Sam Jones",
screenTime: [
{ date: "2019-06-11", usage: { mapMyRun: 100, whatsApp: 0, facebook: 0, safari: 10} },
{ date: "2019-06-13", usage: { mapMyRun: 30, whatsApp: 0, facebook: 0, safari: 16} },
{ date: "2019-06-14", usage: { mapMyRun: 20, whatsApp: 0, facebook: 0, safari: 31} },
]
},
];
expect(getScreentimeAlertList(inputArray,"2019-05-02")).toEqual([])
expect(getScreentimeAlertList(inputArray,"2019-05-02")).not.toBe([]);
});



test("getScreentimeAlertList more than 1 atching record", () => {
    const getScreentimeOutputArray = [ 'Beth Smith', 'Sam Jones' ];
    const inputArray = [
{
username: "beth_1234",
name: "Beth Smith",
screenTime: [
{ date: "2019-05-01", usage: { twitter: 34, instagram: 22, facebook: 40} },
{ date: "2019-06-11", usage: { twitter: 56, instagram: 40, facebook: 31} },
{ date: "2019-05-03", usage: { twitter: 12, instagram: 15, facebook: 19} },
{ date: "2019-05-04", usage: { twitter: 10, instagram: 56, facebook: 61} },
]
},
{
username: "sam_j_1989",
name: "Sam Jones",
screenTime: [
{ date: "2019-06-11", usage: { mapMyRun: 100, whatsApp: 0, facebook: 0, safari: 10} },
{ date: "2019-06-13", usage: { mapMyRun: 30, whatsApp: 0, facebook: 0, safari: 16} },
{ date: "2019-06-14", usage: { mapMyRun: 20, whatsApp: 0, facebook: 0, safari: 31} },
]
}
];
expect(getScreentimeAlertList(inputArray,"2019-06-11")).toEqual(getScreentimeOutputArray)
expect(getScreentimeAlertList(inputArray,"2019-06-11")).not.toBe(getScreentimeOutputArray);
});
});

describe("hextoRGB Conversion", () => {
    test("hex to RGB Conversion", () => {
    expect(hexToRGB("#FF1133")).toBe("rgb(255,17,51)");
    expect(hexToRGB("#F52314")).toBe("rgb(245,35,20)");
    expect(hexToRGB("#000000")).toBe("rgb(0,0,0)");
    expect(hexToRGB("#AA4433")).toBe("rgb(170,68,51)");
    });
});


describe("findWinner", () => {
    test("find Winner naughts and crosses col", () => {
        const inputArray = [
            ["X", "0", null],
            ["X", null, "0"],
            ["X", null, "0"]
        ];
    expect(findWinner(inputArray)).toBe("X");
    });
    test("find Winner naughts and crosses Row", () => {
        const inputArray = [
            ["0", "0", "0"],
            ["X", null, null],
            ["X", null, null]
        ];
    expect(findWinner(inputArray)).toBe("0");
    });

    test("find Winner naughts and crosses Draw", () => {
        const inputArray = [
            ["0", "X", "0"],
            ["X", "X", null],
            [null, "0", null]
        ];
    expect(findWinner(inputArray)).toBe(null);
    });

    test("find Winner naughts and crosses Diagonal", () => {
        const inputArray = [
            ["0", "X", "X"],
            [null, "0", null],
            [null, "X", "0"]
        ];
    expect(findWinner(inputArray)).toBe("0");
    });



    });

